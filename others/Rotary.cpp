#include "rotary.h"

RotaryEncoderButton::RotaryEncoderButton(int pinA, int pinB, int buttonPin) :
  _pinA(pinA), _pinB(pinB), _buttonPin(buttonPin) {}

void RotaryEncoderButton::begin() {
  pinMode(_pinA, INPUT_PULLUP);
  pinMode(_pinB, INPUT_PULLUP);
  pinMode(_buttonPin, INPUT_PULLUP);
  _lastState = digitalRead(_pinA);
  _lastButtonState = digitalRead(_buttonPin);
  _lastDebounceTime = 0;
  _lastButtonTime = 0;
  _value = 0;
  _pressed = false;
  _longPressed = false;
}

void RotaryEncoderButton::update() {
  unsigned long now = millis();

  // Debounce rotary encoder
  int state = digitalRead(_pinA);
  if (state != _lastState) {
    _lastDebounceTime = now;
  }
  if ((now - _lastDebounceTime) > DEBOUNCE_TIME) {
    if (state != _state) {
      _state = state;
      if (digitalRead(_pinB) != state) {
        _value++;
      } else {
        _value--;
      }
    }
  }
  _lastState = state;

  // Debounce button
  int buttonValue = digitalRead(_buttonPin);
  if (buttonValue != _lastButtonState) {
    _lastButtonTime = now;
  }
  if ((now - _lastButtonTime) > DEBOUNCE_TIME) {
    if (buttonValue != _buttonState) {
      _buttonState = buttonValue;
      if (_buttonState == LOW) {
        _pressed = true;
      }
      if (_buttonState == HIGH) {
        if ((now - _lastButtonTime) > LONG_PRESS_TIME) {
          _longPressed = true;
        }
      }
    }
  }
  _lastButtonState = buttonValue;
}

int RotaryEncoderButton::getValue() {
  int value = _value;
  _value = 0;
  return value;
}

bool RotaryEncoderButton::isPressed() {
  bool pressed = _pressed;
  _pressed = false;
  return pressed;
}

bool RotaryEncoderButton::isLongPressed() {
  bool longPressed = _longPressed;
  _longPressed = false;
  return longPressed;
}
