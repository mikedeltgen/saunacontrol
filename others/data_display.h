#ifndef DATA_DISPLAY
#define DATA_DISPLAY
byte get_current_temp();

byte get_current_hum();

void ClearFirstRow();
void ClearSecondRow();
void writeFirstRow();
void writeSecondRow();
void writeThirdRow();
void write4dRow();
void displaySaunaSet();
void displayHeater();
void displayTemperature();
void displayHumidity();
void ValueDisplay();

class DataDisplay{
    public:
        void show();
        void clear();
}

#endif