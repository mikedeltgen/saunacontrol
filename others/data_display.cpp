#include <Arduino.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

byte get_current_temp()
{
    int res = random(100);
    return res;
}

byte get_current_hum()
{
    return random(110);
}

void ClearFirstRow()
{
    lcd.setCursor(0, 0);
    lcd.print("                    ");
}
void ClearSecondRow()
{
    lcd.setCursor(0, 1);
    lcd.print("                    ");
}
void writeFirstRow()
{
    lcd.setCursor(0, 0);
    lcd.print("Temperature: ");
}
void writeSecondRow()
{
    lcd.setCursor(0, 1);
    lcd.print("Humidity: ");
}
void writeThirdRow()
{
    lcd.setCursor(0, 2);
    lcd.print("Heating:            ");
}
void write4dRow()
{
    lcd.setCursor(0, 3);
    lcd.print("                    ");
}
void displaySaunaSet()
{
    if (oldPosition != lastPosition)
    {
        if (millis() - rotary_timer > 50)
        {
            rotary_timer = millis();
            if (oldPosition < lastPosition)
            {
                if (Register.set_temperature > 0)
                {
                    Register.set_temperature -= 1;
                }
            }
            else if (oldPosition > lastPosition)
            {
                if (Register.set_temperature < 100)
                {
                    Register.set_temperature += 1;
                }
            }
            lcd.setCursor(0, 3);
            lcd.print("Set Sauna:      ");
            lcd.setCursor(11, 3);
            lcd.print(Register.set_temperature);
        }
        lastPosition = oldPosition;
    }
}

void displayHeater()
{
    lcd.setCursor(9, 2);
    lcd.print("           ");
    lcd.setCursor(9, 2);
    String heater[3];
    Register.heater_as_text(heater);
    lcd.print(heater[0]);
    lcd.print(" ");
    lcd.print(heater[1]);
    lcd.print(" ");
    lcd.print(heater[2]);
}
void displayTemperature()
{
    lcd.setCursor(13, 0);
    lcd.print("     ");
    lcd.setCursor(13, 0);
    Register.is_temperature = get_current_temp();

    int len = sizeof(String(Register.is_temperature));
    if (len == 2)
    {
        lcd.setCursor(14, 0);
    }
    else if (len == 1)
    {
        lcd.setCursor(13, 0);
    }
    lcd.print(String(Register.is_temperature));
    lcd.print(" C");
    lcd.write(GRAD);
}
void displayHumidity()
{
    lcd.setCursor(10, 1);
    lcd.print("        ");
    lcd.setCursor(10, 1);
    Register.humidity = get_current_hum();
    int len = sizeof(String(Register.humidity));
    if (len == 2)
    {
        lcd.setCursor(10, 1);
    }
    else if (len == 1)
    {
        lcd.setCursor(11, 1);
    }

    lcd.print(String(Register.humidity));
    lcd.print(" %rel");
}

void ValueDisplay()
{
    // Display measured values
    if (millis() - display_value_timer > (second * 3))
    {
        display_value_timer = millis();
        displayTemperature();
        displayHumidity();
        displayHeater();
    }
    displaySaunaSet();
}
