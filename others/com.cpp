#include <Encoder.h>
#include <Arduino.h>

unsigned long serial_timer =0;
unsigned long second = 1000;
unsigned long write_timer = 0;
unsigned long write_delay = 50000;
const byte BOT[4] = {255,255,0,1};
byte REGISTER[3]= {56,80,4};
long oldPosition  = -999;

Encoder myEnc(2, 3);

void setup()
{
    Serial.begin(115200);
    Serial.println("Started");
}

void SerialReader()
{
    if (Serial.available() > 0)
    {
        serial_timer = millis();
        int len = Serial.available();
        char buf[len + 1];
        Serial.readBytes(buf, len);

    }
    if (millis() - serial_timer > (second*5))
    {
        serial_timer = millis();
    }
}

void

void SerialWriter()
{
     if (millis() - write_timer > write_delay)
    {
        write_timer = millis();
        Serial.print(BOT);
        Serial.print(REGISTER);
        Serial.print(BOT);

    }
 
}


void loop()
{
    SerialReader();
    SerialWriter();
    long newPosition = myEnc.read();
    if (newPosition != oldPosition) {
        oldPosition = newPosition;
        Serial.println(newPosition);
    }
}