#include <Arduino.h>
#include <Wire.h>

const byte SAUNA_ADDRESS = 0x14;
const byte RASPBERRY_ADDRESS = 0x01;
const byte VERSION = 0x01;
const int NUM_REGISTERS = 13;
const byte MAX_REC_BYTES = 3;
// Commands
const byte GET_ALL = 255;
const byte GET_TEMP = 100;
const byte SET_TEMP = 101;
const byte GET_HUMIDITY = 200;

const byte GET_LIGHT = 10;
const byte SET_LIGHT = 11;
byte TEMPERATURE = 0;

void update_temperature(byte value)
{
    // set set_temperature to value
    TEMPERATURE = TEMPERATURE + (int)value;
}

void update_light(byte value)
{
    // set light to value
    switch (value)
    {
    case 0:
        // switch light off
        break;
    case 1:
        // switch light on
        break;
    case 3:
        // toggle light
        break;

    default:
        break;
    }
}

byte read_temperature()
{
    return (byte) TEMPERATURE;
}

byte read_set_temperature()
{
    return (byte)56;
}

byte read_light()
{
    return 1;
}

byte read_humidity()
{
    return 68;
}

void i2c_receive(int bytesReceived)
{

    Serial.println("Receiving");
    byte buf[MAX_REC_BYTES];
    int i = 0;
    for (i = 0; i < bytesReceived; i++)
    {
        if (i < MAX_REC_BYTES)
        { // check if above maximum
            buf[i] = Wire.read();
        }
        else
        {
            Wire.read(); // throw away extra
        }
    }
    // Execute the command contained in buf[0]
    switch (buf[0])
    {
    case SET_TEMP:
        update_temperature(buf[3]);
        Serial.println("update Temp");
        Serial.println(read_temperature());
        break;
    case SET_LIGHT:
        update_light(buf[1]);
        Serial.println("update Light");
        break;
    default:
        char s1[4];
        char s2[4];
        char s3[4];
        snprintf(s1, 4, "%d", buf[0]);
        snprintf(s2, 4, "%d", buf[1]);
        snprintf(s3, 4, "%d", buf[2]);

        Serial.println("No Command ");
        Serial.println(s1);
        Serial.println(s2);
        Serial.println(s3);
        break;
    }
    Serial.println("--------------");
}

void i2c_write(byte *buf)
{
    Wire.beginTransmission(RASPBERRY_ADDRESS);
    Wire.write(buf, sizeof buf);
    byte error = Wire.endTransmission();
    if (error)
    {
        Serial.println("Error occurred when writing");
        if (error == 5)
            Serial.println("It was a timeout");
    }
#if defined(WIRE_HAS_TIMEOUT)
    Wire.clearWireTimeoutFlag();
#endif
    // wire error handling could be implemented
    /*
    Returns
        0: success.
        1: data too long to fit in transmit buffer.
        2: received NACK on transmit of address.
        3: received NACK on transmit of data.
        4: other error.
        5: timeout
    */
}

void i2c_request()
{
    Serial.println("Quest : ");
    byte buf[4];
    buf[0] = read_temperature();
    buf[1] = read_set_temperature();
    buf[2] = read_humidity();
    buf[3] = read_light();
    Wire.write(buf, sizeof buf);
    Serial.println(read_temperature());
    Serial.println("*************");

    // byte buf[4];
    // buf[0] = 10;
    // buf[1] = 20;
    // buf[2] = 30;
    // buf[3] = 40;
    // i2c_write(buf);
    // byte command = Wire.read();
    // byte buf[4];
    // Serial.print("Command :");
    // Serial.println(command);
    // Serial.println(Wire.available());
    // switch (command)
    // {
    // case GET_ALL:
    //     // get all values and send them back
    //     buf[0] = read_temperature();
    //     buf[1] = read_set_temperature();
    //     buf[2] = read_humidity();
    //     buf[3] = read_light();
    //     i2c_write(buf);
    //     break;
    // case GET_TEMP:
    //     // get Temp values and send it back
    //     buf[0] = read_temperature();
    //     i2c_write(buf);
    //     break;
    // case GET_HUMIDITY:
    //     // get humidity values and send it back
    //     buf[0] = read_humidity();
    //     i2c_write(buf);
    //     break;
    // case GET_LIGHT:
    //     // get light status and send it back
    //     buf[0] = read_light();
    //     i2c_write(buf);
    //     break;
    // case 0:
    //   byte buf[MAX_REC_BYTES];
    //   buf[0] = SAUNA_ADDRESS;
    //   buf[1] = VERSION;
    //   i2c_write(buf);
    //   break;
    // default:
    //     Serial.println("Done Request.");
    //     break;
    // }
}

void setup()
{
    Serial.begin(115200);
    Wire.begin(SAUNA_ADDRESS);
#if defined(WIRE_HAS_TIMEOUT)
    Wire.setWireTimeout(3000 /* us */, true /* reset_on_timeout */);
#endif
    Wire.onRequest(i2c_request);
    Wire.onReceive(i2c_receive);

    Serial.println("Started");
}

void loop()
{
    // delay(100);
}