#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display
const byte GRAD = 0xdf;
unsigned int second = 1000;
unsigned long display_value_timer = millis();
unsigned long serial_timer = millis();
byte cursor_pos = 0;
byte serial_buffer[128];

String get_current_temp()
{
    int res = random(100);
    return String(res);
}

String get_current_hum()
{
    int res = random(110);
    return String(res);
}
// display all key codes
void displayKeyCodes(void)
{
    uint8_t i = 0;
    while (1)
    {
        lcd.setCursor(0, 2);
        lcd.print("Codes 0x");
        lcd.print(i, HEX);
        lcd.print("-0x");
        lcd.print(i + 15, HEX);
        lcd.setCursor(0, 3);
        for (int j = 0; j < 16; j++)
        {
            lcd.write(i + j);
        }
        i += 16;

        delay(4000);
    }
}

void ClearFirstRow()
{
    lcd.setCursor(0, 0);
    lcd.print("                    ");
}
void ClearSecondRow()
{
    lcd.setCursor(0, 1);
    lcd.print("                    ");
}
void writeFirstRow()
{
    lcd.setCursor(0, 0);
    lcd.print("Temperature: ");
}
void writeSecondRow()
{
    lcd.setCursor(0, 1);
    lcd.print("Humidity: ");
}
void writeThirdRow()
{
    lcd.setCursor(0, 2);
    lcd.print("Heating: off off off");
}
void displayHeater(byte heater)
{
    lcd.setCursor(9, 2);
    lcd.print("           ");
    lcd.setCursor(9, 2);
    /*
    heater byte
    000 : off off off
    001 : on off off
    010 : off on off
    100 : off off on
    ...
    */
    String h1 = "off";
    String h2 = "off";
    String h3 = "off";
    if (bitRead(heater, 1) == 1)
    {
        h1 = "on";
    }
    if (bitRead(heater, 2) == 1)
    {
        h2 = "on";
    }
    if (bitRead(heater, 3) == 1)
    {
        h3 = "on";
    }

    lcd.print(h1);
    lcd.print(" ");
    lcd.print(h2);
    lcd.print(" ");
    lcd.print(h3);
}
void displayTemperature()
{
    lcd.setCursor(13, 0);
    lcd.print("     ");
    lcd.setCursor(13, 0);
    String temperature = get_current_temp();
    int len = sizeof(temperature);
    if (len == 2)
    {
        lcd.setCursor(14, 0);
    }
    else if (len == 1)
    {
        lcd.setCursor(13, 0);
    }
    lcd.print(temperature);
    lcd.print(" C");
    lcd.write(GRAD);
}
void displayHumidity()
{
    lcd.setCursor(10, 1);
    lcd.print("        ");
    lcd.setCursor(10, 1);
    String temperature = get_current_hum();
    int len = sizeof(temperature);
    if (len == 2)
    {
        lcd.setCursor(10, 1);
    }
    else if (len == 1)
    {
        lcd.setCursor(11, 1);
    }

    lcd.print(temperature);
    lcd.print(" %rel");
}

void ValueDisplay()
{
    // Display measured values
    if (millis() - display_value_timer > (second * 3))
    {
        display_value_timer = millis();
        displayTemperature();
        displayHumidity();
        byte heater = random(7);
        displayHeater(heater);
    }
}

void setup()
{
    Serial.begin(115200);
    Serial.println("Started");
    lcd.init(); // initialize the lcd
    lcd.backlight();
    lcd.clear();
    lcd.home();
    lcd.noAutoscroll();
    lcd.print("Booted.");
    lcd.cursor_on();
    delay(500);
    lcd.cursor_off();
    lcd.clear();
    lcd.blink_off();
    lcd.home();
    writeFirstRow();
    writeSecondRow();
    writeThirdRow();
}

void SerialReader()
{
    if (Serial.available() > 0)
    {
        serial_timer = millis();
        int len = Serial.available();
        char buf[len + 1];
        Serial.readBytes(buf, len);
        lcd.setCursor(cursor_pos,3);
        for (byte cnt = 0; cnt < sizeof(buf)-1; cnt++)
        {
            cursor_pos +=1;
            if (cursor_pos == 19){
                cursor_pos = 0;
            }
            lcd.print(buf[cnt]);
        }

    }
    if (millis() - serial_timer > (second*5))
    {
        serial_timer = millis();
        cursor_pos = 0;
        lcd.setCursor(0, 3);
        lcd.print("                    ");
    }
}

void loop()
{
    ValueDisplay();
    SerialReader();
}