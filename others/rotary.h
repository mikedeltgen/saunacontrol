#ifndef ROTARY_ENCODER_H
#define ROTARY_ENCODER_H

#include <Arduino.h>

class RotaryEncoderButton {
public:
  RotaryEncoderButton(int pinA, int pinB, int buttonPin);
  void begin();
  void update();
  int getValue();
  bool isPressed();
  bool isLongPressed();

private:
  const unsigned long LONG_PRESS_TIME = 1000; // milliseconds
  const unsigned long DEBOUNCE_TIME = 50; // milliseconds

  int _pinA;
  int _pinB;
  int _buttonPin;
  int _state;
  int _lastState;
  int _buttonState;
  int _lastButtonState;
  unsigned long _lastDebounceTime;
  unsigned long _lastButtonTime;
  int _value;
  bool _pressed;
  bool _longPressed;
};

#endif
