#include <Arduino.h>
#include "proto.h"
# include "ini.h"
struct DataPair
{
  char *data;
  size_t length;
};

// Define the special characters
byte START_OF_TRANSMISSION[3] = {0xAA, 0xBB, 0xCC};
byte END_OF_TRANSMISSION[3] = {0xDD, 0xEE, 0xFF};
byte CMD_DELIMITER = ':';

// Maximum size of the data buffer
const int MAX_DATA_LENGTH = 32;

// define possible commands
byte SET_SAUNA_REGISTER[2] = {'S', 'R'};
byte SET_SAUNA_TEMP[2] = {'S', 'T'};
byte SET_HEATER_STATE[2] = {'S', 'H'};
byte SET_SAUNA_ACTIVE[2] = {'S', 'A'};
byte SET_LIGHTS[2] = {'S', 'L'};
byte OPERATION[2] = {'O', 'P'};
byte SERVER_ERROR[2] = {'S', 'E'};
char ERROR[3] = {'E', 'R', 'R'};
char ACK[3] = {'A', 'C', 'K'};

byte GET_SAUNA_REGISTER[2] = {'G', 'R'};
byte GET_SAUNA_HUMIDITY[2] = {'G', 'H'};
byte GET_SAUNA_TEMPERATURE[2] = {'G', 'T'};
byte GET_SAFETY_TEMPERATURE[3] = {'G', 'S', 'T'};

// Function to calculate CRC-16
uint16_t calculateCRC(const char *data, size_t length, uint16_t initialCRC)
{
  uint16_t crc = initialCRC;

  for (size_t i = 0; i < length; i++)
  {
    crc ^= (static_cast<uint16_t>(data[i]) << 8); // XOR with the next byte

    for (int j = 0; j < 8; j++)
    {
      if (crc & 0x8000)
      {
        crc = (crc << 1) ^ 0x1021; // Bitwise left shift and XOR with polynomial
      }
      else
      {
        crc <<= 1; // Bitwise left shift
      }
    }
  }

  return crc;
}

DataPair concatenateVariables(
    const char cmd[], size_t cmd_length,
    const char payload[], size_t payload_length,
    const byte cmd_id)
{
  // Calculate the total length of the concatenated data
  size_t total_length = cmd_length + payload_length + 1 + 2;

  // Create a buffer to hold the concatenated data
  char *concatenated_data = new char[total_length+5];

  // Copy each variable into the concatenated_data buffer
  size_t offset = 0;

  memcpy(concatenated_data + offset, cmd, cmd_length);
  offset += cmd_length;

  concatenated_data[offset++] = CMD_DELIMITER;

  concatenated_data[offset++] = payload_length;

  memcpy(concatenated_data + offset, payload, payload_length);
  offset += payload_length;

  concatenated_data[offset++] = cmd_id;

  // Return the concatenated data and its length as a pair
  DataPair result;
  result.data = concatenated_data;
  result.length = total_length;

  return result;
}

size_t getCharArrayLength(const char *charArray)
{
  size_t length = 0;

  // Iterate through the array until a null-terminating character is encountered
  while (charArray[length] != '\0')
  {
    length++;
  }

  return length;
}

// ProtocolHandler::ProtocolHandler()
// {
//   // constructor
// }

void ProtocolHandler::begin(
    Stream *connection,
    unsigned long int timeout,
    int bufferSize
    // SaunaRegister &saunaRegister)
)
{
  ProtocolHandler::serialPort = connection;
  ProtocolHandler::readTimeout = timeout;
  ProtocolHandler::bufferSize = bufferSize;
  ProtocolHandler::bufferPosition = 0;
  ProtocolHandler::state = STATES::WAITING_FOR_BOT;
  ProtocolHandler::transmission_start = millis();
  ProtocolHandler::readTimeout = 7000;
  ProtocolHandler::data[32] = {};
  ProtocolHandler::saunaRegister = saunaRegister;
}

void ProtocolHandler::resetBuffer()
{
  memset(receiveBuffer, 0, MAX_DATA_LENGTH);
  bufferPosition = 0;
}

void ProtocolHandler::sendCommand(const char *cmd, const char *data, const size_t data_length)
{

  size_t cmd_length = getCharArrayLength(cmd);
  DataPair result = concatenateVariables(
      cmd, cmd_length, data, data_length, ProtocolHandler::commandId);
  char *concatenated_data = result.data;
  size_t total_length = result.length;
  // calculate CRC
  uint16_t crcValue = 0xFFFF;
  crcValue = calculateCRC(concatenated_data, total_length, crcValue);
  uint8_t crcBytes[2];
  crcBytes[0] = static_cast<uint8_t>((crcValue >> 8) & 0xFF); // High byte
  crcBytes[1] = static_cast<uint8_t>(crcValue & 0xFF);

  concatenated_data[total_length++] = crcBytes[0];
  concatenated_data[total_length++] = crcBytes[1];

  // Implementation of sendCommand goes here.
  serialPort->write(START_OF_TRANSMISSION, 3);
  serialPort->write(concatenated_data, total_length);
  serialPort->write(END_OF_TRANSMISSION, 3);
  delete[] result.data;
}

bool ProtocolHandler::receiveCommand(char *cmd, uint8_t &length, uint8_t *data)
{
  // Implementation of receiveCommand goes here.
  return true;
}

bool ProtocolHandler::compareWithBuffer(byte arr[], int length)
{
  for (int i = 0; i < length; i++)
  {
    if (arr[i] != receiveBuffer[i])
    {
      return false;
    }
  }
  return true;
}

bool ProtocolHandler::processData()
{
  // process incoming data
  byte receivedByte = 0;
  if (ProtocolHandler::transmission_start + ProtocolHandler::readTimeout < millis())
  {
    resetBuffer();
    while (serialPort->available() > 0)
    {
      serialPort->read();
    }
    ProtocolHandler::transmission_start = millis();
    return false;
  }

  if (serialPort->available() > 0)
  {
    receivedByte = serialPort->read();
    ProtocolHandler::receiveBuffer[bufferPosition] = receivedByte;
    bufferPosition++;

    switch (state)
    {
    case STATES::WAITING_FOR_BOT:
      if (bufferPosition == 3)
      {
        if (compareWithBuffer(START_OF_TRANSMISSION, 3) == true)
        {
          state = STATES::WAITING_FOR_COMMAND;
          ProtocolHandler::transmission_start = millis();
        };
        resetBuffer();
      }
      break;
    case STATES::WAITING_FOR_COMMAND:
      if (receivedByte == CMD_DELIMITER)
      {
        memcpy(ProtocolHandler::receivedCommand, receiveBuffer, bufferPosition - 1);
        state = STATES::WAITING_FOR_LENGTH;
        resetBuffer();
      }
      break;
    case STATES::WAITING_FOR_LENGTH:
      dataLength = receivedByte;
      state = STATES::WAITING_FOR_DATA;
      resetBuffer();
      break;
    case STATES::WAITING_FOR_DATA:
      if (bufferPosition == dataLength)
      {
        memcpy(
          ProtocolHandler::data,
          receiveBuffer,
          sizeof(ProtocolHandler::data));
        // strncpy(
        //     ProtocolHandler::data,
        //     (char *)receiveBuffer,
        //     sizeof(ProtocolHandler::data));
        state = STATES::WAITING_FOR_COMMAND_ID;
        resetBuffer();
      }
      break;
    case STATES::WAITING_FOR_COMMAND_ID:
      ProtocolHandler::commandId = receivedByte;
      state = STATES::WAITING_FOR_CRC;
      resetBuffer();
      break;
    case STATES::WAITING_FOR_CRC:
      if (bufferPosition == 2)
      {
        memcpy(
            ProtocolHandler::received_crc,
            receiveBuffer,
            2);

        state = STATES::WAITING_FOR_EOT;
        resetBuffer();
      }
      break;
    case STATES::WAITING_FOR_EOT:
      if (bufferPosition == 3)
      {
        if (compareWithBuffer(END_OF_TRANSMISSION, 3) == true)
        {
          state = STATES::HANDLE_COMMAND;
          resetBuffer();
          return true;
        };
      }
      break;
    default:
      break;
    }
  }
  return false;
}

bool ProtocolHandler::compareCommand(byte cmd[], int length)
{
  for (int i = 0; i < length; i++)
  {
    if (cmd[i] != ProtocolHandler::receivedCommand[i])
    {
      return false;
    }
  }
  return true;
}

int ProtocolHandler::handleCommand()
{

  if (state == STATES::HANDLE_COMMAND)
  {

    DataPair result = concatenateVariables(
        ProtocolHandler::receivedCommand, 
        getCharArrayLength(ProtocolHandler::receivedCommand),
        ProtocolHandler::data, ProtocolHandler::dataLength,
        ProtocolHandler::commandId);

    // calculate CRC
    uint16_t crcValue = 0xFFFF;
    // char foo[6] = {'S','A',':',0x01,';',0x01};
    crcValue = calculateCRC(result.data, result.length, crcValue);
    delete[] result.data;
    uint8_t crcBytes[2];
    crcBytes[0] = static_cast<uint8_t>((crcValue >> 8) & 0xFF); // High byte
    crcBytes[1] = static_cast<uint8_t>(crcValue & 0xFF);

    if (
        (crcBytes[0] != static_cast<uint8_t>(ProtocolHandler::received_crc[0])) | 
        (crcBytes[1] != static_cast<uint8_t>(ProtocolHandler::received_crc[1]))
      )
    {
      sendCommand("CE", (char *)crcBytes, 2);
      // sendCommand("ERR", ProtocolHandler::data, ProtocolHandler::dataLength);
      // sendCommand("CE", result.data, sizeof(result.data));
      ProtocolHandler::state = STATES::WAITING_FOR_BOT;
      // reset data buffer
      memset(ProtocolHandler::data, 0, 32);
      memset(ProtocolHandler::receivedCommand, 0, 6);
      return 0;
    }

    if (compareCommand(GET_SAUNA_REGISTER, 2))
    {
      char reg[9];
      ProtocolHandler::saunaRegister.as_bytes(reg);
      sendCommand("ACK", reg, 8);
      
    }
    else if (compareCommand(SET_HEATER_STATE, 2))
    {
      saunaRegister.heaterFromByte(data[0]);
      char res[1] = {static_cast<char>(saunaRegister.heaterToByte())};
      sendCommand("ACK", res, 1);
      
    }
    else if (compareCommand(SET_LIGHTS, 2))
    {
      saunaRegister.lightFromByte(data[0]);
      char res[1] = {static_cast<char>(saunaRegister.lightToByte())};
      sendCommand("ACK", res, 1);
      
    }
    else if (compareCommand(SET_SAUNA_TEMP, 2))
    {
      saunaRegister.set_temperature = data[0];
      char res[1] = {static_cast<char>(saunaRegister.set_temperature)};
      sendCommand("ACK", res, 1);
      
    }
    else if (compareCommand(SET_SAUNA_ACTIVE, 2))
    {
      saunaRegister.active = (static_cast<int16_t>(data[0]) << 8) | (static_cast<uint8_t>(data[1])); // Combine two bytes into one int16_t
      char res[2] = {static_cast<char>(saunaRegister.active >> 8), static_cast<char>(saunaRegister.active & 0xFF)}; // Split int16_t into two bytes      if (res[0] == 0)
      if (saunaRegister.active == 0)
      {
        sendCommand("ACK", 0x00, 2);
        
      }
      else
      {
        sendCommand("ACK", res, 2);
      }
      
    }
    else if (compareCommand(OPERATION, 2))
    {
      char msg[21] = "operational. v.";
      strcat(msg,VERSION);
      sendCommand("ACK", msg, 21);
      
    }
    else if (compareCommand(SERVER_ERROR, 2))
    {
      sendCommand("ERR", "Server Error", 12);
      return 1;
    }
    else
    {
      sendCommand("ERR", "Command Error", 13);
    }
  }
  ProtocolHandler::state = STATES::WAITING_FOR_BOT;
  // reset data buffer
  memset(ProtocolHandler::data, 0, 32);
  memset(ProtocolHandler::receivedCommand, 0, 6);
  return 0;
}