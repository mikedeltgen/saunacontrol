#ifndef INI_H
#define INI_H

//**********************
//** Sauna Controller **
//**********************
#define VERSION "3.0.0"

//**********************
//** I2c              **
//**********************
#define SLC A5
#define SDA A4

//**********************
//** BME280           **
//**********************
#define BME_ADDRESS 0x40

//**********************
//** HD44780 LCD      **
//**********************
#define SAUNA_LCD_ADDRESS 0x27


//*************************
//** Rotary Encoder pins **
//*************************
const byte ROTARY_PINa = 2;
const byte ROTARY_PINb = 3;
const byte ROTARY_BUTTON = 5;

// Additional Characters

//**************************
//** One Wire  Addresses  **
//**************************

#define KILL_SWITCH ef157ab21
#endif