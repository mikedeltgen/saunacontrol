#ifndef DISPLAY_PAGE
#define DISPLAY_PAGE

#include <Arduino.h>
#include "sauna.h"

struct Page
{
    char row1[21];
    char row2[21];
    char row3[21];
    char row4[21];
    byte row=0;
    byte col=0;
};

Page DataPage(SaunaRegister& reg);
Page SettingsPage(SaunaRegister& reg);
Page LightControl(SaunaRegister& reg);
Page StartPage(SaunaRegister& reg);
Page ErrorPage(SaunaRegister& reg);

#endif