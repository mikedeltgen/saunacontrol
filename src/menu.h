#ifndef MENU_DISPLAY_H
#define MENU_DISPLAY_H
#include <Arduino.h>
#include "sauna.h"
#include "page.h"
#include <LiquidCrystal_I2C.h>

// Display Menu


enum MENU_ITEM
{
    DATA_DISPLAY = 0,
    TEMPERATURE_SET = 1,
    LIGHT_CONTROL = 2,
};

class MenuDisplay {
    private:
        enum class Mode {
            DisplayPage,
            SelectPage,
            EditPage
        };
        
        SaunaRegister& saunaRegister;
        LiquidCrystal_I2C& lcd;
        MENU_ITEM SelectedPage = DATA_DISPLAY;
        MENU_ITEM oldSelectedPage = DATA_DISPLAY;
        int RotaryPosition = 0;
        long int LastRotaryValue = 0;
        
        // this one is the current button state
        bool buttonState = false;
        // this one is the toggling button state
        long int buttonTimer = millis();
        long int editTimer = millis();
        long int displayTimer = millis();
        long int RotaryTimer = millis();
        long int display_value_timer = millis();
        byte cursorPosition = 0;
        void setMode(Mode);
        void current();
        Mode mode_;
    public:
        unsigned long editTimeout = 5000;
        unsigned long displayTimeout = 10000;
        Page CurrentPage = DataPage(saunaRegister);
        MenuDisplay(SaunaRegister& reg, LiquidCrystal_I2C& lcddisp) :
         saunaRegister(reg), lcd(lcddisp), mode_(Mode::DisplayPage) {
        }
        void rotary_changed(long int rotary);
        void handleButtonPressed();
        void menu_selector();
        void edit_toggle();
        void enable_select();
        void disable_select();
        void show();
        void showStartPage();
        void showErrorPage();
};


#endif