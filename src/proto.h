#ifndef PROTO_HANDLER_H
#define PROTO_HANDLER_H
#include <Arduino.h>
#include "sauna.h"

enum STATES
{
  WAITING_FOR_BOT,
  WAITING_FOR_COMMAND,
  WAITING_FOR_LENGTH,
  WAITING_FOR_DATA,
  WAITING_FOR_COMMAND_ID,
  WAITING_FOR_CRC,
  WAITING_FOR_EOT,
  HANDLE_COMMAND
};

class ProtocolHandler
{
    public:
      
      Stream *serialPort;
      unsigned long readTimeout;
      unsigned long transmission_start;
      int bufferSize;
      int bufferPosition;
      SaunaRegister& saunaRegister;
      STATES state;
      void resetBuffer();
      byte receiveBuffer[64];
      char receivedCommand[6] = {0};
      byte commandId;
      byte dataLength;
      char data[33];
      char crc[2];
      char received_crc[2];
      ProtocolHandler(SaunaRegister& reg) :
         saunaRegister(reg) {
        };
      void begin(
        Stream* connection, 
        unsigned long int timeout, 
        int bufferSize);
      // Send data using the custom protocol
      void sendCommand(const char* cmd, const char* data, const size_t);
      bool processData();
      int handleCommand();
      void copyCMD();
      // Receive and process data using the custom protocol
      bool receiveCommand(char* cmd, uint8_t& length, uint8_t* data);
      bool compareWithBuffer(byte arr[], int length);
      bool compareCommand(byte cmd[], int length);
};

#endif
