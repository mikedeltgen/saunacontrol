#include <MemoryFree.h>
#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include "ini.h"
#include "sauna.h"
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include "menu.h"
#include "page.h"
#include "proto.h"

LiquidCrystal_I2C lcd(SAUNA_LCD_ADDRESS, 20, 4);
SaunaRegister SaunaReg = *new SaunaRegister();
MenuDisplay Menu = *new MenuDisplay(SaunaReg, lcd);
Encoder myEnc(ROTARY_PINa, ROTARY_PINb);
ProtocolHandler proto = *new ProtocolHandler(SaunaReg);


#define TINY_BME280_I2C
#include "TinyBME280.h"
#include <OneWire.h>
tiny::BME280 bmeSensor;

OneWire MasterTemp(6);

MENU_ITEM CurrentState = DATA_DISPLAY;
byte LAST_BUTTON_STATE = HIGH;

unsigned int second = 1000;
unsigned long emitter_timer = millis();
unsigned long int display_value_timer = millis();
unsigned long int button_timer = millis();
unsigned long send_data_timer = millis();

// measure_timer is used to give the OneWire time to measure
unsigned long measure_timer = millis();
// is the OneWire measuring 
bool measuring = false;

const uint8_t SAFETY_SAUNA_TEMPERATURE_ADDRESS[8] = {0x28, 0x58, 0x0, 0x81, 0xe3, 0xa2, 0x3c, 0x86};

void read_master()
{
    byte i;
    byte present = 0;
    byte data[9];
    float celsius;
    present = MasterTemp.reset();
    MasterTemp.select(SAFETY_SAUNA_TEMPERATURE_ADDRESS);
    MasterTemp.write(0xBE); // Read Scratchpad

    for (i = 0; i < 9; i++)
    { // we need 9 bytes
        data[i] = MasterTemp.read();
    }

    int16_t raw = (data[1] << 8) | data[0];
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00)
        raw = raw & ~7; // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20)
        raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40)
        raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
    celsius = (float)raw / 16.0;
    // Serial.print("P=");
    // Serial.print(present, HEX);
    // Serial.print(" ");
    // Serial.print(" CRC=");
    // Serial.print(OneWire::crc8(data, 8), HEX);
    // Serial.println();
    // Serial.print(" Celsius, ");
    // Serial.print(celsius);
    // Serial.println();
}

void get_current_temp()
{
    int res = bmeSensor.readFixedTempC() / 100;
    // randomSeed(analogRead(0)); // Initialize random number generator
    // int res = random(-30, 30);
    // if (res < -40 || res > 130)
    // {
    //     res = 0;
    // }
    int8_t normalized_value = res + 40;

    int8_t byteRes = static_cast<uint8_t>(normalized_value * 255/150);
    SaunaReg.is_temperature = byteRes;
}

void get_current_hum()
{
    int res = bmeSensor.readFixedHumidity() / 1000;
    SaunaReg.humidity = res;
}
void setup()
{
    lcd.init(); // initialize the LCD
    lcd.backlight();
    lcd.clear();
    lcd.home();
    lcd.blink_off();
    lcd.noAutoscroll();
    Serial.begin(115200);
    proto.begin(&Serial,5000,128);
    // Serial.println("Started");
    // Serial.println("Loading EEPROM data.");
    SaunaReg.load();
    // Serial.println("EEPROM Data loaded.");
    
    MasterTemp.reset();
    MasterTemp.select(SAFETY_SAUNA_TEMPERATURE_ADDRESS);
    MasterTemp.write(0x44, 1); // start conversion, with parasite power on at the end
    delay(2000);

    bmeSensor.setI2CAddress(0x76); // This is also the default value
    if (bmeSensor.begin() == false){
        char msg[24] = "Sensor A connect failed";
        proto.commandId = 0xff;
        proto.sendCommand("ERR",msg,23);
    }
    bmeSensor.setTempOverSample(1);     // 0 to 16 are valid. 0 disables temp sensing. See table 24.
    bmeSensor.setPressureOverSample(1); // 0 to 16 are valid. 0 disables pressure sensing. See table 23.
    bmeSensor.setHumidityOverSample(1); // 0 to 16 are valid. 0 disables humidity sensing. See table 19.

    bmeSensor.setMode(tiny::Mode::NORMAL);

    pinMode(ROTARY_BUTTON, INPUT);
    digitalWrite(ROTARY_BUTTON, HIGH);
    LAST_BUTTON_STATE = digitalRead(ROTARY_BUTTON);
    char msg[18] = "Controller booted";
    proto.commandId = 0xbb;
    proto.sendCommand("ACK",msg,17);
    char msg2[21] = "operational. v.";
    strcat(msg2,VERSION);
    proto.sendCommand("ACK", msg2, 21);
    Menu.showStartPage();
}

void loop()
{
    long newPosition = myEnc.read();
    Menu.rotary_changed(newPosition);
    Menu.handleButtonPressed();
    Menu.show();
    delay(0);
    if (millis() - send_data_timer > (second * 5))
    {
        send_data_timer = millis();
        get_current_temp();
        get_current_hum();
        // read_master();
        char dataToSend[9];
        SaunaReg.as_bytes(dataToSend);
        proto.commandId = 0xaa;
        proto.sendCommand("REG", dataToSend,8);
    };
    if (proto.processData()) {
        // got data from server
        if (proto.handleCommand() > 0){
            // command was handled
            Menu.show();
        }else{
            Menu.showErrorPage();
        }
    };
    delay(0);
}