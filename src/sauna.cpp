// Sauna Register Class
#include <Arduino.h>
#include "sauna.h"
#include <EEPROM.h>

struct PromRegister
{
    byte set_temperature;
};

unsigned char SaunaRegister::lightToByte()
{
    unsigned char c = 0;
    for (int i = 0; i < 8; i++)
    {
        if (lights[i])
        {
            c |= 1 << i;
        }
    }
    return c;
}
void SaunaRegister::lightFromByte(unsigned char c)
{
    for (int i = 0; i < 8; ++i)
        lights[i] = (c & (1 << i)) != 0;
}
unsigned char SaunaRegister::heaterToByte()
{
    unsigned char c = 0;
    for (int i = 0; i < 3; i++)
    {
        if (heater[i])
        {
            c |= 1 << i;
        }
    }
    return c;
}
void SaunaRegister::heater_as_text(char c[][4])
{
    for (int i = 0; i < 3; i++)
    {
        if (heater[i])
        {
            strcpy(c[i], "On");
        }
        else
        {
            strcpy(c[i], "Off");
        }
    }
}
void SaunaRegister::light_as_text(char c[][4])
{
    for (int i = 0; i < 8; i++)
    {
        if (lights[i])
        {
            strcpy(c[i], "On");
        }
        else
        {
            strcpy(c[i], "Off");
        }
    }
}
void SaunaRegister::heaterFromByte(unsigned char c)
{
    // the Heater is a 1 byte value 
    // bit 0: Heater 1
    // bit 1: Heater 2
    // bit 2: Heater 3
    for (int i = 0; i < 3; ++i)
        heater[i] = (c & (1 << i)) != 0;
}
void SaunaRegister::as_bytes(char *data)
{
    data[0] = is_temperature;
    data[1] = humidity;
    data[2] = heaterToByte();
    data[3] = set_temperature;
    data[4] = oven_temperature;
    data[5] = lightToByte();
    data[6] = (active >> 8) & 0xFF; // High byte
    data[7] = active & 0xFF; // Low byte
}

void SaunaRegister::from_bytes(char *data)
{
    heaterFromByte(data[2]);
    set_temperature = data[3];
    lightFromByte(data[5]);
    active = (static_cast<int16_t>(data[6]) << 8) | 
             (static_cast<uint8_t>(data[7]));
}

void SaunaRegister::save()
{

    PromRegister saveData;
    saveData.set_temperature = set_temperature;

    // Read the previous data from EEPROM
    PromRegister prevData;
    EEPROM.get(0, prevData);

    // Only save the new data if it is different from the previous data
    if (memcmp(&saveData, &prevData, sizeof(PromRegister)) != 0)
    {
        // Serial.println("Saving to EEPROM.");
        EEPROM.put(0, saveData);
        EEPROM.end();
    };
}

void SaunaRegister::load()
{
    PromRegister eepromData;
    EEPROM.get(0, eepromData);
    set_temperature = eepromData.set_temperature;
    EEPROM.end();
}