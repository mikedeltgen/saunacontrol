// Sauna Register 

#ifndef SAUNA_REGISTER_H
#define SAUNA_REGISTER_H



class SaunaRegister {
    public:
        SaunaRegister(){};
        int16_t active = 0;
        int8_t is_temperature = 0;
        byte humidity = 0;
        bool heater[3] = {false,false,false};
        byte set_temperature = 0;
        byte oven_temperature = 0;
        bool lights[8] = {false,false,false,false,false,false,false,false};
        bool BUTTON_STATE = false;
        bool EDIT_MODE = false;
        unsigned char lightToByte();
        void lightFromByte(unsigned char c);
        unsigned char heaterToByte();
        void heaterFromByte(unsigned char c);
        void heater_as_text(char c[][4]);
        void light_as_text(char c[][4]);
        void as_bytes(char *data);
        void from_bytes(char *data);
        void save();
        void load();
};
#endif