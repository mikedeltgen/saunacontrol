#include <Arduino.h>
#include "menu.h"
#include "ini.h"

//this method sets the current mode
void MenuDisplay::setMode(Mode mode)
{
    mode_ = mode;
    editTimeout = millis();
    displayTimeout = millis();
}


// this method sets the current displayed Page
void MenuDisplay::current()
{
    if (millis() - editTimer > editTimeout){
        // return to display page if no change
        mode_ = Mode::DisplayPage;
        saunaRegister.save();
    }
    switch (SelectedPage)
    {
    case DATA_DISPLAY:
    {
        /* code */
        CurrentPage = DataPage(saunaRegister);
        break;
    }
    case TEMPERATURE_SET:
    {
        if (mode_ == Mode::EditPage)
        {
            int temperatureValue = saunaRegister.set_temperature;
            temperatureValue += RotaryPosition;
            RotaryPosition = 0;
            if (temperatureValue > 100)
            {
                temperatureValue = 40;
            }
            else if (temperatureValue < 0)
            {
                temperatureValue = 100;
            }
            saunaRegister.set_temperature = temperatureValue;
            
        }
        else if (mode_ == Mode::DisplayPage)
        {
            
            // only saves if values have changed
            saunaRegister.save();
        }
        CurrentPage = SettingsPage(saunaRegister);
        // set cursor to position 0,13
        CurrentPage.row = 0;
        CurrentPage.col = 13;
        break;
    }
    case LIGHT_CONTROL:
    {
        byte cursorPositions[9][2] = {
            {0, 1},  // Light 1
            {4, 1},  // Light 2
            {8, 1},  // Light 3
            {12, 1}, // Light 4
            {0, 2},  // Light 5
            {4, 2},  // Light 6
            {8, 2},  // Light 7
            {12, 2}, // Light 8
            {0, 3}   // Close/End
        };
        if (mode_ == Mode::SelectPage)
        {
            cursorPosition = (cursorPosition + RotaryPosition + 10) % 10;
                }
        if (cursorPosition == 8 && mode_ == Mode::EditPage)
        {
            // end was selected 
            setMode(Mode::DisplayPage);
        }
        if (mode_ == Mode::EditPage){
            saunaRegister.lights[cursorPosition] =! saunaRegister.lights[cursorPosition];
            setMode(Mode::SelectPage);
        }
        RotaryPosition = 0;
        CurrentPage = LightControl(saunaRegister);
        CurrentPage.col = cursorPositions[cursorPosition][0];
        CurrentPage.row = cursorPositions[cursorPosition][1];
        break;
    }
    default:
    {
        /* code */
        CurrentPage = DataPage(saunaRegister);
        break;
    }
    }
}

// handle button is pressed
void MenuDisplay::handleButtonPressed()
{
    if (millis() - buttonTimer > 150)
    {
            
        if (digitalRead(ROTARY_BUTTON) == LOW && !buttonState)
        {
            buttonState = true;
            switch (mode_)
            {
            case Mode::DisplayPage:
                if (SelectedPage == MENU_ITEM::TEMPERATURE_SET)
                {
                    setMode(Mode::EditPage);
                    // Serial.println("Edit Mode");
                    lcd.blink_on();
                    lcd.cursor_on();
                    editTimer = millis();
                }
                else if (SelectedPage == MENU_ITEM::LIGHT_CONTROL)
                {
                    setMode(Mode::SelectPage);
                    // Serial.println("Select Mode");
                    lcd.blink_off();
                    lcd.cursor_on();
                }
                else if (SelectedPage == MENU_ITEM::DATA_DISPLAY)
                {
                    setMode(Mode::DisplayPage);
                    // Serial.println("Display Mode");
                    lcd.blink_off();
                    lcd.cursor_off();
                }
                break;
            case Mode::SelectPage:
                setMode(Mode::EditPage);
                // Serial.println("Edit Mode");
                lcd.blink_on();
                lcd.cursor_on();

                editTimer = millis();
                break;
            case Mode::EditPage:
                if (SelectedPage == MENU_ITEM::TEMPERATURE_SET)
                {
                    setMode(Mode::DisplayPage);
                    // Serial.println("Display Mode");
                    lcd.blink_off();
                    lcd.cursor_off();
                }
                else if (SelectedPage == MENU_ITEM::LIGHT_CONTROL)
                {
                    setMode(Mode::SelectPage);
                    // Serial.println("Select Mode");
                    lcd.blink_off();
                    lcd.cursor_on();
                }
                break;
            }
        }
        buttonTimer = millis();
       
    }else{
        if (buttonState == true)
        {
            buttonState = false;
        }
    }
}


/**
 * @brief Handles changes in the rotary encoder value.
 * 
 * This method is called whenever the rotary encoder value changes. It checks if the new value
 * is different from the previous value, and if so, it updates the RotaryPosition variable
 * based on the direction of the change. If the change is greater than 4, it sets the RotaryPosition
 * to 1 or -1, depending on the direction of the change, and also updates the editTimer variable
 * with the current millis() value.
 * 
 * @param newRotaryValue The new value of the rotary encoder.
 */
void MenuDisplay::rotary_changed(long int newRotaryValue)
{
    if (LastRotaryValue != newRotaryValue)
    {
        if (LastRotaryValue - newRotaryValue > 4)
        {
            RotaryPosition = 1;
            editTimer = millis();
        }
        else if (newRotaryValue - LastRotaryValue > 4)
        {
            RotaryPosition = -1;
            editTimer = millis();
        }
        LastRotaryValue = newRotaryValue;
    }
}

// Select menu item to be displayed
void MenuDisplay::menu_selector()
{
    if (millis() - displayTimer > displayTimeout){
        mode_ = Mode::DisplayPage;
        saunaRegister.save();
        SelectedPage = MENU_ITEM::DATA_DISPLAY;
    }
    
    switch (mode_)
    {
    case Mode::DisplayPage:
        if (SelectedPage + RotaryPosition > MENU_ITEM::LIGHT_CONTROL)
        {
            SelectedPage = MENU_ITEM::DATA_DISPLAY;
        }
        else if (SelectedPage + RotaryPosition < MENU_ITEM::DATA_DISPLAY)
        {
            SelectedPage = MENU_ITEM::LIGHT_CONTROL;
        }
        else
        {
            SelectedPage = static_cast<MENU_ITEM>(SelectedPage + RotaryPosition);
        }
        RotaryPosition = 0;
        
        break;
    default:
        break;
    }
    
}

void MenuDisplay::show()
{
    // display_value_timer
    if (millis() - display_value_timer > 100)
    {
        // check if menu needs to be changed
        menu_selector();
        // update current page
        current();
        // check if we need to toggle into select mode
        if (oldSelectedPage != SelectedPage)
        {
            oldSelectedPage = SelectedPage;
            displayTimer = millis();
            lcd.clear();
        }
        Page page = CurrentPage;
        lcd.setCursor(0, 0);
        lcd.print(page.row1);
        lcd.setCursor(0, 1);
        lcd.print(page.row2);
        lcd.setCursor(0, 2);
        lcd.print(page.row3);
        lcd.setCursor(0, 3);
        lcd.print(page.row4);

        switch (mode_)
        {
        case Mode::SelectPage:
            lcd.setCursor(page.col, page.row);
        default:
            break;
        }
        display_value_timer = millis();
    }
};

void MenuDisplay::showStartPage()
{
    Page page = StartPage(saunaRegister);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(page.row1);
    lcd.setCursor(0, 1);
    lcd.print(page.row2);
    lcd.setCursor(0, 2);
    lcd.print(page.row3);
    lcd.setCursor(0, 3);
    lcd.print(page.row4);
    delay(2000);
}


void MenuDisplay::showErrorPage()
{
    Page page = ErrorPage(saunaRegister);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(page.row1);
    lcd.setCursor(0, 1);
    lcd.print(page.row2);
    lcd.setCursor(0, 2);
    lcd.print(page.row3);
    lcd.setCursor(0, 3);
    lcd.print(page.row4);
    delay(2000);
}