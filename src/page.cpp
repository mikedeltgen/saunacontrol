// using namespace std;
#include <Arduino.h>
#include "sauna.h"
#include "page.h"
#include "ini.h"

void row_filler(char *buffer)
{
    int len_buffer = strlen(buffer);
    len_buffer = strlen(buffer);
    int chrPlus = 20 - len_buffer;
    for (int i = 0; i < chrPlus; i++)
    {
        buffer[len_buffer + i] = ' ';
    }
}

Page DataPage(SaunaRegister &reg)
{
    // this functions returns the 4 LCD lines for the default data view
    // as `Page` Structure
    char buffer[21] = "";
    memset(buffer, '\0', sizeof(buffer));

    struct Page page;
    char temperature[4];
    itoa(reg.is_temperature, temperature, 10);
    strcat(buffer, "Temperature: ");
    strcat(buffer, temperature);
    strcat(buffer, " C");
    strcat(buffer, "\xDF");
    row_filler(buffer);
    strcpy(page.row1, buffer);

    memset(buffer, '\0', sizeof(buffer));
    char humidity[4];
    itoa(reg.humidity, humidity, 10);
    strcat(buffer, "Humidity: ");
    strcat(buffer, humidity);
    strcat(buffer, " %");
    row_filler(buffer);
    strcpy(page.row2, buffer);

    memset(buffer, '\0', sizeof(buffer));
    char heater[3][4];
    reg.heater_as_text(heater);
    strcat(buffer, "Heating: ");
    int j = strlen(buffer);
    for (int i = 0; i < 3; i++)
    {
        strcpy(buffer + j, heater[i]);
        j += strlen(heater[i]);
        if (i < 2)
        {
            strcpy(buffer + j, " ");
            j += 1;
        }
    }
    row_filler(buffer);
    strcpy(page.row3, buffer);
    memset(buffer, '\0', sizeof(buffer));
    if (reg.active > 0)
    {
        strcat(buffer, "active rest: ");
        int hours = (reg.active >> 12) & 0x0F; // Extract hours
        int minutes = (reg.active >> 6) & 0x3F; // Extract minutes
        int seconds = reg.active & 0x3F; // Extract seconds
        char rest[10];
        sprintf(rest, "%d:%02d:%02d", hours, minutes, seconds); // Format as time
        strcat(buffer, rest);
    }
    row_filler(buffer);
    strcpy(page.row4, buffer);
    return page;
}

Page SettingsPage(SaunaRegister &reg)
{
    /* this function returns the Settings menu

        Sauna set temperature : xx

    */
    struct Page page;
    char buffer[21] = "";
    char temperature[4];
    itoa(reg.set_temperature, temperature, 10);

    strcat(buffer, "Temperature: ");
    strcat(buffer, temperature);
    strcat(buffer, " C");
    strcat(buffer, "\xDF");
    row_filler(buffer);
    strcpy(page.row1, buffer);
    memset(buffer, '\0', sizeof(buffer));
    row_filler(buffer);
    strcpy(page.row2, buffer);
    strcpy(page.row3, buffer);
    strcpy(page.row4, buffer);
    return page;
}

Page LightControl(SaunaRegister &reg)
{
    /* this function returns the Light Control menu

    */

    char buffer[21] = "";
    char light[8][4];
    reg.light_as_text(light);
    struct Page page;
    memset(buffer, '\0', 21);
    strcat(buffer, "Lights: ");
    row_filler(buffer);
    strcpy(page.row1, buffer);

    memset(buffer, '\0', 21);
    int j = 0;
    for (int i = 0; i < 4; i++)
    {
        strcpy(buffer + j, light[i]);
        j += strlen(light[i]);
        if (i < 4)
        {
            strcpy(buffer + j, " ");
            j += 1;
        }
    }
    row_filler(buffer);
    strcpy(page.row2, buffer);

    memset(buffer, '\0', 21);
    j = 0;
    for (int i = 4; i < 8; i++)
    {
        strcpy(buffer + j, light[i]);
        j += strlen(light[i]);
        if (i < 8)
        {
            strcpy(buffer + j, " ");
            j += 1;
        }
    }
    row_filler(buffer);
    strcpy(page.row3, buffer);
    
    memset(buffer, '\0', 21);
    strcat(buffer, "end");
    row_filler(buffer);
    strcpy(page.row4, buffer);

    return page;
}

Page StartPage(SaunaRegister &reg)
{
    /* this function returns Start Page

    */

    char version[6] = {VERSION};

    char buffer[21] = "";
    struct Page page;
    memset(buffer, '\0', 21);
    strcat(buffer, "Sauna Control");
    row_filler(buffer);
    strcpy(page.row1, buffer);

    memset(buffer, '\0', 21);
    strcat(buffer, "version ");
    strcat(buffer, version);
    row_filler(buffer);
    strcpy(page.row2, buffer);

    memset(buffer, '\0', 21);
    strcat(buffer, "by mbM 2023 ");
    row_filler(buffer);
    strcpy(page.row3, buffer);
    memset(buffer, '\0', sizeof(buffer));
    row_filler(buffer);
    strcpy(page.row4, buffer);
    return page;
}

Page ErrorPage(SaunaRegister &reg)
{
    /* this function returns an Error Page

    */

    char version[6] = {VERSION};

    char buffer[21] = "";
    struct Page page;
    memset(buffer, '\0', 21);
    strcat(buffer, "Sauna Control");
    row_filler(buffer);
    strcpy(page.row1, buffer);

    memset(buffer, '\0', 21);
    strcat(buffer, "Server Error");
    strcat(buffer, version);
    row_filler(buffer);
    strcpy(page.row2, buffer);

    memset(buffer, '\0', 21);
    strcat(buffer, "Please restart");
    row_filler(buffer);
    strcpy(page.row3, buffer);
    memset(buffer, '\0', sizeof(buffer));
    row_filler(buffer);
    strcpy(page.row4, buffer);
    return page;
}