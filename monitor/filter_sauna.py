from platformio.public import DeviceMonitorFilterBase
import time

BOT = bytearray([0xAA, 0xBB, 0xCC])
EOT = bytearray([0xDD, 0xEE, 0xFF])

def decimal_to_bytearray(decimal_value):
    hex_string = hex(decimal_value)[2:]  # Convert to hexadecimal and remove the "0x" prefix
    hex_string = hex_string.zfill(6)    # Ensure the string is 6 characters long with leading zeros
    byte_values = [int(hex_string[i:i+2], 16) for i in range(0, len(hex_string), 2)]
    return bytearray(byte_values)
        
class Demo(DeviceMonitorFilterBase):
    NAME = "sauna"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.receive_buffer = bytearray()
        self.start_time = int(time.monotonic())
        print("Demo filter is loaded")

    def rx(self, text):
        tokens = text.split()
        byte_values = [int(_,16) for _ in tokens]
        b = bytearray(byte_values)
        self.receive_buffer.extend(b)
        if BOT in self.receive_buffer and EOT in self.receive_buffer:
            receive_time = int(time.monotonic())
            timestamp = receive_time - self.start_time 
            eot = self.receive_buffer.index(EOT)
            bot = self.receive_buffer.index(BOT)
            ret = f"[{timestamp:6}] DATA: {self.receive_buffer[bot:eot+3]}\n"
            self.receive_buffer = self.receive_buffer[eot+3:]
            return ret
        return ""

    def tx(self, text):
        print(f"Sent: {text}\n")
        return text